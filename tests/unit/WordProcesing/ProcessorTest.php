<?php
namespace String\WordProcessing;

use PHPUnit\Framework\TestCase;

class ProcessorTest extends TestCase
{
    private const STRING_NOT_CHANGED_1 = 'Lorem ipsum dolor sit amet';
    private const STRING_NOT_CHANGED_2 = "Lorem\nipsum";

    private const STRING_BREAK_AT_BOUNDARIES_1_BEFORE = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Anter';
    private const STRING_BREAK_AT_BOUNDARIES_1_AFTER = "Lorem ipsum dolor\nsit amet,\nconsectetur\nadipiscing elit, sed\ndo eiusmod tempor\nincididunt ut labore\net dolore magna\naliqua. Anter";

    private const STRING_BREAK_AT_BOUNDARIES_2_BEFORE = 'Lorem ipsumdolorsitamet, consecteturadipiscing elit, seddoeiusmod temporincididuntutlabore et dolore magna aliqua. Anter';
    private const STRING_BREAK_AT_BOUNDARIES_2_AFTER = "Lorem\nipsumdolorsitamet, c\nonsecteturadipiscing\nelit, seddoeiusmod t\nemporincididuntutlab\nore et dolore magna\naliqua. Anter";

    private const STRING_BREAK_AT_BOUNDARIES_3_BEFORE = 'L o r e m i p s u m d o l o r s i t';
    private const STRING_BREAK_AT_BOUNDARIES_3_AFTER = "L\no\nr\ne\nm\ni\np\ns\nu\nm\nd\no\nl\no\nr\ns\ni\nt";

    private const STRING_BREAK_AT_BOUNDARIES_4_BEFORE = 'L o r e m i p s u m d o l o r s i t';
    private const STRING_BREAK_AT_BOUNDARIES_4_AFTER = "L o\nr e\nm i\np s\nu m\nd o\nl o\nr s\ni t";

    private const STRING_BREAK_WORD_1_BEFORE = 'Loremipsumdolorsitametconsecteturadipiscing ante';
    private const STRING_BREAK_WORD_1_AFTER = "Loremipsumdolorsitam\netconsecteturadipisc\ning ante";

    private const STRING_BREAK_WORD_2_BEFORE = "Lorem ipsumdolorsitametconsecteturadipiscing ante\nconset";
    private const STRING_BREAK_WORD_2_AFTER = "Lorem ipsumdolorsita\nmetconsecteturadipis\ncing ante\nconset";

    private const STRING_BREAK_WORD_3_BEFORE = 'Loremipsumdolorsitametconsecteturadipiscing';
    private const STRING_BREAK_WORD_3_AFTER = "L\no\nr\ne\nm\ni\np\ns\nu\nm\nd\no\nl\no\nr\ns\ni\nt\na\nm\ne\nt\nc\no\nn\ns\ne\nc\nt\ne\nt\nu\nr\na\nd\ni\np\ni\ns\nc\ni\nn\ng";

    /**
     * @dataProvider stringShouldNotBeChangedIfDoesNotExceedLengthDataProvider
     * @test
     */
    public function stringShouldNotBeChangedIfDoesNotExceedLength(string $string, int $length): void
    {
        $this->assertSame($string, Processor::wrap($string, $length));
    }

    public function stringShouldNotBeChangedIfDoesNotExceedLengthDataProvider(): array
    {
        return [
            [self::STRING_NOT_CHANGED_1, 75],
            [self::STRING_NOT_CHANGED_2, 5],
        ];
    }

    /**
     * @dataProvider stringShouldBeBreakIntoNewLinesAtWordBoundariesDataProvider
     * @test
     */
    public function stringShouldBreakIntoNewLinesAtWordBoundaries(string $string, string $result, int $length): void
    {
        $this->assertEquals($result, Processor::wrap($string, $length));
    }

    public function stringShouldBeBreakIntoNewLinesAtWordBoundariesDataProvider(): array
    {
        return [
            [self::STRING_BREAK_AT_BOUNDARIES_1_BEFORE, self::STRING_BREAK_AT_BOUNDARIES_1_AFTER, 20],
            [self::STRING_BREAK_AT_BOUNDARIES_2_BEFORE, self::STRING_BREAK_AT_BOUNDARIES_2_AFTER, 20],
            [self::STRING_BREAK_AT_BOUNDARIES_3_BEFORE, self::STRING_BREAK_AT_BOUNDARIES_3_AFTER, 1],
            [self::STRING_BREAK_AT_BOUNDARIES_4_BEFORE, self::STRING_BREAK_AT_BOUNDARIES_4_AFTER, 3],
        ];
    }

    /**
     * @dataProvider stringShouldBreakIntoNewLinesInsideWordDataProvider
     * @test
     */
    public function stringShouldBreakIntoNewLinesInsideWord(string $string, string $result, int $length): void
    {
        $this->assertEquals($result, Processor::wrap($string, $length));
    }

    public function stringShouldBreakIntoNewLinesInsideWordDataProvider(): array
    {
        return [
            [self::STRING_BREAK_WORD_1_BEFORE, self::STRING_BREAK_WORD_1_AFTER, 20],
            [self::STRING_BREAK_WORD_2_BEFORE, self::STRING_BREAK_WORD_2_AFTER, 20],
            [self::STRING_BREAK_WORD_3_BEFORE, self::STRING_BREAK_WORD_3_AFTER, 1],
        ];
    }
}
