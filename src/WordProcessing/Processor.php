<?php
namespace String\WordProcessing;

use InvalidArgumentException;

class Processor
{
    /**
     * Function breaks line if it exceed defined length.
     * It breaks at word boundaries, unless word is exceeding defined length,
     * then it breaks word into pieces.
     *
     * @param string $string
     * @param int $length
     * @return string
     */
    public static function wrap(string $string, int $length): string
    {
        if ($length < 1) {
            throw new InvalidArgumentException('Length should be at least 1');
        }

        if (mb_strlen($string) < $length) {
            return $string;
        }

        //analyze text line by line
        $stringLines = explode("\n", $string);

        $resultString = '';
        foreach ($stringLines as $key => $line) {
            if (mb_strlen($line) <= $length) {
                //whole line length fits length restrictions - add line and break line if its not last line
                $resultString .= $line;
                if ($key !== array_key_last($stringLines)) {
                    $resultString .= "\n";
                }
                continue;
            }

            //process text word by word
            $words = explode(' ', $line);
            $lineLength = 0;
            foreach ($words as $wordKey => $word) {
                $wordLength = mb_strlen($word);
                //if word length including space and already built line is exceeding defined length
                //and word doesn't need to be split, add new line
                if ($lineLength + $wordLength + 1 > $length && $wordLength < $length) {
                    $resultString .= "\n";
                    $lineLength = 0;
                }
                if ($lineLength + $wordLength + 1 <= $length) {
                    //word can still fit into the line
                    if ($lineLength !== 0) {
                        //add space if its not first word in line
                        $resultString .= ' ';
                        $lineLength++;
                    }
                    $resultString .= $word;
                    $lineLength += $wordLength;
                    continue;
                }
                $wordLengthUsed = 0;
                if ($lineLength > 0) {
                    if ($lineLength > $length - 2) {
                        //if line doesn't fit at least: space and one character from broken word, add new line
                        $resultString .= "\n";
                        $lineLength = 0;
                    } else {
                        //if line fits piece of broken word, add space
                        $resultString .= " ";
                        $lineLength++;
                    }
                }
                //break word into pieces and fit in lines
                while ($wordLengthUsed < $wordLength) {
                    //add piece of the word that length is equal to available space
                    $partToAdd = mb_substr($word, $wordLengthUsed, $length - $lineLength);
                    $resultString .= $partToAdd;
                    $wordLengthUsed += mb_strlen($partToAdd);
                    if ($wordLengthUsed < $wordLength) {
                        //if word wasn't fully added to text, break line
                        $resultString .= "\n";
                        $lineLength = 0;
                    } else {
                        $lineLength = mb_strlen($partToAdd);
                    }
                }
            }
            if ($key !== array_key_last($stringLines)) {
                $resultString .= "\n";
            }

        }

        return $resultString;
    }
}
